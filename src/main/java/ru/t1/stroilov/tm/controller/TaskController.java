package ru.t1.stroilov.tm.controller;

import ru.t1.stroilov.tm.api.controller.ITaskController;
import ru.t1.stroilov.tm.api.service.ITaskService;
import ru.t1.stroilov.tm.model.Task;
import ru.t1.stroilov.tm.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[SHOW TASKS]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (final Task task : tasks) {
            System.out.printf("%s. %s - %s \n", index, task.getName(), task.getDescription());
            index++;
        }
        System.out.println("[END]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[ERROR]");
        else System.out.println("[TASK CREATED]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[DELETE TASKS]");
        taskService.deleteAll();
        System.out.println("[TASKS DELETED]");
    }
}
