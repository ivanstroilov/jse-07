package ru.t1.stroilov.tm.controller;

import ru.t1.stroilov.tm.api.controller.IProjectController;
import ru.t1.stroilov.tm.api.service.IProjectService;
import ru.t1.stroilov.tm.model.Project;
import ru.t1.stroilov.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[SHOW PROJECTS]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (final Project project : projects) {
            System.out.printf("%s. %s - %s \n", index, project.getName(), project.getDescription());
            index++;
        }
        System.out.println("[END]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null) System.out.println("[ERROR]");
        else System.out.println("[PROJECT CREATED]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[DELETE PROJECTS]");
        projectService.deleteAll();
        System.out.println("[PROJECTS DELETED]");
    }
}
