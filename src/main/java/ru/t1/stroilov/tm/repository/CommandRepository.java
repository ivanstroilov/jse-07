package ru.t1.stroilov.tm.repository;

import ru.t1.stroilov.tm.api.repository.ICommandRepository;
import ru.t1.stroilov.tm.constant.AppConstant;
import ru.t1.stroilov.tm.constant.ArgumentConstant;
import ru.t1.stroilov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command VERSION = new Command(
            AppConstant.VERSION, ArgumentConstant.VERSION, "Display program version."
    );

    private static final Command HELP = new Command(
            AppConstant.HELP, ArgumentConstant.HELP, "Display list of terminal commands."
    );

    private static final Command INFO = new Command(
            AppConstant.INFO, ArgumentConstant.INFO, "Display developer info."
    );

    private static final Command COMMANDS = new Command(
            AppConstant.COMMANDS, ArgumentConstant.COMMANDS, "Display allowed commands."
    );

    private static final Command ARGUMENTS = new Command(
            AppConstant.ARGUMENTS, ArgumentConstant.ARGUMENTS, "Display allowed arguments."
    );

    private static final Command EXIT = new Command(
            AppConstant.EXIT, null, "Terminate the application."
    );

    private static final Command PROJECT_LIST = new Command(
            AppConstant.PROJECT_LIST, null, "Show all Projects."
    );

    private static final Command PROJECT_CREATE = new Command(
            AppConstant.PROJECT_CREATE, null, "Create a Project."
    );

    private static final Command PROJECT_DELETE = new Command(
            AppConstant.PROJECT_DELETE, null, "Delete all Projects."
    );

    private static final Command TASK_LIST = new Command(
            AppConstant.TASK_LIST, null, "Show all Tasks."
    );

    private static final Command TASK_CREATE = new Command(
            AppConstant.TASK_CREATE, null, "Create a Task."
    );

    private static final Command TASK_DELETE = new Command(
            AppConstant.TASK_DELETE, null, "Delete all Tasks."
    );

    private static final Command[] COMMANDS_ARRAY = new Command[]{
            VERSION, HELP, INFO, COMMANDS, ARGUMENTS,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_DELETE,
            TASK_LIST, TASK_CREATE, TASK_DELETE,
            EXIT
    };

    public Command[] getCommandsArray() {
        return COMMANDS_ARRAY;
    }
}
