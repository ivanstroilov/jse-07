package ru.t1.stroilov.tm.api.repository;

import ru.t1.stroilov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    List<Project> findAll();

    void deleteAll();

}
