package ru.t1.stroilov.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void createProject();

    void clearProjects();

}
